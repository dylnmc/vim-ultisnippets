# vim-snippets
another vim-snippets (dylnmc-flavored)

## Plugin Manager

* Plug
    - Minimal Vimrc

```vim
Plug 'dylnmc/vim-ultisnippets
```

* Other plugin managers
        - Refer to your plugin manager's documentation to see how to install this plugin
